package view;

import java.util.Iterator;
import java.util.Scanner;

import controller.Controller;
import data_structures.MinPQ;
import data_structures.Queue;
import model.Interseccion;
import model.InterseccionT;
import model.InterseccionVM;

public class View 
{
	private Scanner in;
	private Controller controlador;
	private boolean activo;

	public View()
	{
		in = new Scanner(System.in);
		controlador = new Controller();
		activo = false;
	}

	public void printMenu()
	{
		print("0. Crear Grafo con los datos de mallas viales de Bogota ");
		print("1. Agregar la información de costo.");
		print("2. Cargar grafoJson");
		print("3. Crear grafo con los datos del archivo JSON generado");
		print("4. Encontrar el camino de costo mínimo (menor tiempo promedio según Uber en la ruta)");
		print("5. Encontrar el camino de menor costo (menor distancia Haversine) para un viaje entre dos localizaciones geográficas");
		print("6. Calcular un árbol de expansión mínima (MST) con criterio distancia, utilizando el algoritmo de Prim");
		print("7. Encontrar el camino de menor costo (menor distancia Haversine) para un viaje entre dos localizaciones geográficas");
		print("8.Construir un nuevo grafo simplificado No dirigido de las zonas Uber, donde cada zona (MOVEMENT_ID) es representada con un único vértice y los enlaces entre ellas representan su vecindad dentro de la malla vial.");
		print("9.Calcular un árbol de expansión mínima (MST) con criterio distancia, utilizando el algoritmo de Kruskal");
		print("10.Calcular el camino de costo mínimo (algoritmo de Dijkstra) basado en el tiempo promedio entre una zona de origen y una zona de destino sobre el grafo de zonas.");
		print("11.calcular los caminos de menor longitud (cantidad de arcos) a todas sus zonas alcanzables.");
		print("12.calcular los caminos de menor longitud (cantidad de arcos) a todas sus zonas alcanzables.");
		print("13. Exit");
		print("<Codigos auxiliares>");
		print("90. Mostrar mapa");
		print("91. Mostrar el mas cercano");
		print("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
	}

	public void print(String mensaje) {
		System.out.println(mensaje);
	}

	public String read() {
		return in.nextLine();
	}

	public int leerOpcion() {
		return Integer.parseInt(read());
	}

	public void Start()
	{
		activo = true;
		while(activo)
		{
			printMenu();
			int op = leerOpcion();
			print("---------------------------");
			print("---------------------------");
			long time = System.currentTimeMillis();
			accion(op);
			time = System.currentTimeMillis() - time;
			print("---------------------------");
			print("Tiempos transcurrido: "+time+" ms");
			print("---------------------------");
		}
	}

	private void accion(int op)
	{
		switch (op) 
		{
		case 0:


			controlador.CargarArchivos(); break;
		case 1:	controlador.CargarTiempos(); break;
		case 2: controlador.CargarGrafoJSON(); break;
		case 3: controlador.GenerarGrafoJSON(); break;
		case 4: 
			print("Ingrese La latitud de la interseccion de origen");
			double platitudOrigen = Double.parseDouble(read());       
			print("Ingrese  La Longitud de la interseccion de origen ");
			double plongitudOreigen = Double.parseDouble(read());

			print("Ingrese  La latitud de la interseccion de Destino ");
			double platitudDestino = Double.parseDouble(read());
			print("Ingrese  La Longitud de la interseccion de Destino  ");
			double plongitudDestino = Double.parseDouble(read());
			
			Iterator<Integer> iter = controlador.R4(platitudOrigen,plongitudOreigen,platitudDestino,plongitudDestino); 
			
			
			int numvertices=0;
			double costominimo4=0;
			double distaciaMinima4=0;
			
			while(iter.hasNext()!=false )
			{
				int ID = (int) iter.next();
				
				Interseccion aAgregar = controlador.darInterseccion(ID);

				
				if(aAgregar!=null)
				{
					numvertices++;
					
					int iDvertice4 =aAgregar.getIDNodo();
					double lat4 =aAgregar.getLatitud();
					double long4 =aAgregar.getLongitud();
					
					print("El vertice con ID:"+iDvertice4+" Su latitud es: "+lat4 +" Su longitud es: "+long4+"\n---------------" );
					
				}

			}
			
			
			print("La cantidad total de vértices es"+numvertices+"\n---------------" );
			print("El costo minimo en segundo es: "+costominimo4+"\n---------------" );
			print("La distancia estimada en km es: "+distaciaMinima4+"\n---------------" );
			
			break;
			
		case 5: 
			print("Ingrese la cantidad de vertices que desea ver");
			int n5 = Integer.parseInt(read()); 
			controlador.R5(n5); 
		break;
		
		case 6: 
			
			System.out.println("--------- \n Calculando MTS con el algoritmo de Prim !! \n---------"); 
			
			Interseccion[] totalverticesComponente6= controlador.R6(); 
			
			print("El total de vértices en el componente:"+ totalverticesComponente6+"\n---------------");

			
			int verticesIdentificadores6 =0;	
			int	idVerticeinial6=0;
			int	idVerticefinal6=0;
			double  costoTotal = 0;
			
			
			print("Los vertices identificadores son:"+ verticesIdentificadores6+"\n---------------");
			
			
			print("El arco numero:"+0+ "tiene como vertice inicial"+idVerticeinial6+" y tiene como Vertice Final:" +idVerticefinal6 + "\n---------------");
			
			print("El costo total en km es de:"+ costoTotal+"\n---------------");
			

		break;
		case 7:
			
			System.out.println("--------- \n Digite la laitud y longitud del vertice de origen y del de destino"+"\n---------------");

			print("Ingrese La latitud de la interseccion de origen");
			int platitudOrigen7 = Integer.parseInt(read());       
			print("Ingrese  La Longitud de la interseccion de origen ");
			int plongitudOreigen7 = Integer.parseInt(read());
			
			print("Ingrese  La latitud de la interseccion de Destino ");
			int platitudDestino7 = Integer.parseInt(read());
			print("Ingrese  La Longitud de la interseccion de Destino  ");
			int plongitudDestino7 = Integer.parseInt(read());
			
			
			controlador.R7(platitudOrigen7, plongitudOreigen7,platitudDestino7,plongitudDestino7); 
			
			int numvertices7=0;
			double costominimo7=0;
			double distaciaMinima7=0;
			
			print("La cantidad total de vértices es"+numvertices7+"\n---------------" );
			
			for (int i = 0; i < numvertices7; i++) 
			{
				int iDvertice7 =0;
				double lat7 =0;
				double long7 =0;
				
				print("El vertice con ID:"+iDvertice7+" Su latitud es: "+lat7 +" Su longitud es: "+long7+"\n---------------" );
			}

			
			print("El costo minimo en segundo es: "+costominimo7+"\n---------------" );
			print("La distancia estimada en km es: "+distaciaMinima7+"\n---------------" );

			

		break;
		case 8:
			print("Ingrese la latitud");
			double lat = Double.parseDouble(read());
			print("Ingrese la longitud");
			double lon = Double.parseDouble(read());
			print("Ingrese el tiempo");
			double tiempo = Double.parseDouble(read());
			for(InterseccionT inter : controlador.verticesAlcanzables(lat, lon, tiempo)){
				print(inter.getInterseccio()+" Tiempo: "+inter.gettiempo());
			}
			
		break;
		case 9:
			System.out.println("--------- \n Calculando MTS con el algoritmo de Kruskal !! \n---------"); 
			
			  
			
			controlador.R9(); 
			
			int totalverticesComponente9=0;
			
			print("El total de vértices en el componente:"+ totalverticesComponente9+"\n---------------");
			
			
			
			int verticesIdentificadores9 =0;	
			int	idVerticeinial9=0;
			int	idVerticefinal9=0;
			double  costoTotal9 = 0;
			
			
			print("Los vertices identificadores son:"+ verticesIdentificadores9+"\n---------------");
			print("El arco numero:"+0+ "tiene como vertice inicial"+idVerticeinial9+" y tiene como Vertice Final:" +idVerticefinal9 + "\n---------------");
			print("El costo total en km es de:"+ costoTotal9+"\n---------------");
			
			

		break;
		case 10:
			
			controlador.R10(); 

		break;
		case 11:
			
			controlador.R11(); 

		break;
		case 12:
			
			controlador.R12(); 
			
			int numArcos=0;
			for (int i = 0; i < 80; i++) 
			{
				
				int idvertice=0;
				int idezona= 0;
				
				print("Secuencia de vertice:"+ idvertice +" con zona:"+idezona+"\n---------------");		
			}
			print("El numero total de arcos es:"+ numArcos +"\n---------------");
			

		break;


		case 13: Close(); break;

		case 90: controlador.MostrarMapa(); break;
		case 91: masCercanoA(); break;
		default: print(">>>>>> Opcion no valida <<<<<<");	break;
		}
	}

	private void masCercanoA()
	{
		print("Ingrese la latitud");
		double lat = Double.parseDouble(read());
		print("Ingrese la longitud");
		double lon = Double.parseDouble(read());
		print("El nodo m�s cercano es:");
		int id = controlador.DarIDMasCercano(lat, lon);
		print("id: "+id);
		print(""+controlador.darInterseccion(id));
	}


	public void Close()
	{
		in.close();
		activo = false;
	}

}
