package controller;

import com.teamdev.jxmaps.*;
import com.teamdev.jxmaps.Polygon;
import com.teamdev.jxmaps.swing.MapView;

import data_structures.Grafo;
import data_structures.LinearProbingHashST;
import model.Costo;
import model.Interseccion;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Random;

/**
 * Esta clase representa los objetos y metodos basicos
 * necesarios para la realizacion de talleres y proyectos
 * que tengan que ver con dibujo de mapas
 * @author Juan Pablo Cano
 */
public class Maps extends MapView
{
    //-------------------------
    // Atributos
    //------------------------

    /**
     * Mapa cargado de JxMaps
     */
    private Map map;

    /**
     * Tabla de Hash que contiene las universidades en llaves
     * y las Latitudes y Longitudes en Valor
     */
    private LinearProbingHashST<Integer, LatLng> table;

    /**
     * Arreglo de latitudes y longitudes
     */
    private LatLng[] locs;
    
    private LinearProbingHashST<Integer, String> colores;

    private Grafo<Integer, Interseccion, Costo> grafo;
    //-----------------------
    // Constructores
    //------------------------

    /**
     *
     */
    public Maps(Grafo<Integer, Interseccion, Costo> grafo, boolean monocromatico)
    {
        table = new LinearProbingHashST<Integer, LatLng>(grafo.getCantNodos()+2);
        locs = new LatLng[grafo.getCantNodos()];
        colores = new LinearProbingHashST<>(Controller.CANTIDAD_ZONAS+2);
        this.grafo = grafo;
        try
        {
            // El archivo de locaciones
            int i = 0;
            for(Interseccion intersecion : grafo.getValues())
            {
            	int idNodo = intersecion.getIDNodo();
                double lat = intersecion.getLatitud();
                double lon = intersecion.getLongitud();
                LatLng latLng = new LatLng(lat, lon);
                table.put(idNodo, latLng);
                locs[i] = latLng;
                if(colores.get(intersecion.getMOVEMENT_ID()) == null)
                	colores.put(intersecion.getMOVEMENT_ID(), getColor(monocromatico));
                i++;
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }

        // Se espera que JxMaps cargue el mapa
        setOnMapReadyHandler(new MapReadyHandler() {
            @Override
            public void onMapReady(MapStatus status)
            {
                if(status == MapStatus.MAP_STATUS_OK)
                {
                    map = getMap();
                    initMap(map);
                    /*
                     * Puede cambiar la funcion por otra
                     * Por ejemplo drawPolygon(Map map)
                     */
                    drawCircle(map);
                }
            }
        });
    }
    
    private String getColor(boolean monocromatico)
    {
    	if(monocromatico)
    		return "#000000";
    	Random ran = new Random();
    	int r = ran.nextInt(255);
    	int g = ran.nextInt(255);
    	int b = ran.nextInt(255);
    	return String.format("#%02x%02x%02x", r, g, b);
    }

    /**
     * Metodo que se encarga de dibujar un marcador de posicion
     * @param map Lienzo donde se colocar� el marcador
     */
    public void drawMarker(Map map)
    {
        for(int id : table.keys())
        {
            LatLng loc = table.get(id);
            Marker marker = new Marker(map);
            marker.setPosition(loc);

            InfoWindow infoWindow = new InfoWindow(map);
            infoWindow.setContent("Hola "+ id);
            infoWindow.open(map, marker);
        }
    }

    /**
     * Metodo que se encarga de dibujar una linea con
     * el comportamiento de un poligono
     * @param map Lienzo donde se colocar�n las lineas
     */
    public void drawPolygon(Map map)
    {
        PolygonOptions po = new PolygonOptions();
        // Color de relleno
        po.setFillColor("#FF5733");
        // Opacidad de relleno
        po.setFillOpacity(0.35);

        // Los dos extremos del poligono
        LatLng[] path = new LatLng[2];
        int i = 0;

        for(int id : table.keys())
        {
            path[0] = locs[i];
            path[1] = locs[i+1];
            // Objeto de latitud y longitud
            LatLng loc = table.get(id);
            // Marcador
            Marker marker = new Marker(map);
            marker.setPosition(loc);

            // Ventana de informacion
            InfoWindow infoWindow = new InfoWindow(map);
            infoWindow.setContent("Hola "+ id);
            infoWindow.open(map, marker);

            // Poligono
            Polygon polygon = new Polygon(map);
            // Se le agrega el camino, debe ser un arreglo de LatLng
            polygon.setPath(path);
            // Se colocan las opciones
            polygon.setOptions(po);
            i++;
        }
    }
    
    public void resaltarPunto(int id)
    {    	
    	CircleOptions co = new CircleOptions();
        // Opacidad de relleno
        co.setFillOpacity(1);
        // Ancho de l�nea
        co.setStrokeWeight(1);
        // Opacidad de linea
        co.setStrokeOpacity(1);
        
        String color = "#FF0000";
        // Color de linea
        co.setStrokeColor(color);
        co.setFillColor(color);
        
        // Objeto de latitud y longitud
        LatLng loc = table.get(id);
        // Creaci�n del c�rculo
        Circle circle = new Circle(map);
        // Centro
        circle.setCenter(loc);
        // Opciones
        circle.setOptions(co);
        // Radio
        circle.setRadius(100);
    }

    /**
     * M�todo que se encarga de dibujar los circulos
     * @param map Lienzo donde se colocar�n los circulos
     */
    public void drawCircle(Map map)
    {
        for(int id : table.keys())
        {
        	// Las opciones de creaci�n del c�rculo
            CircleOptions co = new CircleOptions();
            // Opacidad de relleno
            co.setFillOpacity(0.35);
            // Ancho de l�nea
            co.setStrokeWeight(1);
            // Opacidad de linea
            co.setStrokeOpacity(0.2);
            
            String color = colores.get(grafo.getVertice(id).getMOVEMENT_ID());
            // Color de linea
            co.setStrokeColor(color);
            co.setFillColor(color);
            
            // Objeto de latitud y longitud
            LatLng loc = table.get(id);
            // Creaci�n del c�rculo
            Circle circle = new Circle(map);
            // Centro
            circle.setCenter(loc);
            // Opciones
            circle.setOptions(co);
            // Radio
            circle.setRadius(10);
            
        }
    }

    /**
     * Inicialia el mapa de JxMaps
     * @param map El mapa que ser� inicializado
     */
    public void initMap(Map map)
    {
        // Las opciones del mapa
        MapOptions mapOptions = new MapOptions();
        // Los Controles del mapa
        MapTypeControlOptions controlOptions = new MapTypeControlOptions();
        mapOptions.setMapTypeControlOptions(controlOptions);

        // Centro del mapa
        map.setCenter(new LatLng(4.6012, -74.0657));
        // Zoom del Mapa
        map.setZoom(11.0);
    }

    /**
     * Inicializa el marco donde el mapa se va a cargar
     * @param titulo El t�tulo del marco
     */
    public void initFrame(String titulo)
    {
        JFrame frame = new JFrame(titulo);
        frame.setSize(700, 500);
        frame.setVisible(true);
        frame.add(this, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * Main method
     * @param args Parametros no necesarios
     */
//    public static void main(String[] args)
//    {
//        Maps maps = new Maps();
//        maps.initFrame("Hola mundo");
//    }
}