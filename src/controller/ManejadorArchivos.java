package controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import data_structures.Grafo;
import model.Costo;
import model.Haversine;
import model.Interseccion;

public class ManejadorArchivos {

	public final String RUTA_BASE = "C:/Users/usuario/Documents/Malla vial archivos/";
	public final String BOGOTA_ARCOS = RUTA_BASE + "bogota_arcos.txt";
	public final String BOGOTA_VERTICES = RUTA_BASE + "bogota_vertices.txt";
	public final String TIEPOS_VIEAJES = RUTA_BASE + "bogota-cadastral-2018-1-WeeklyAggregate (1).csv";
	public final String GRAFO = RUTA_BASE + "Grafo.json";

//	public final String RUTA_BASE = "C:/Users/usuario/Documents/Malla vial archivos/";
//	public final String BOGOTA_ARCOS = "./data/bogota_arcos.txt";
//	public final String BOGOTA_VERTICES = "./data/bogota_vertices.txt";
//	public final String TIEPOS_VIEAJES = "./data/bogota-cadastral-2018-1-WeeklyAggregate.csv";
//	public final String GRAFO = RUTA_BASE + "Grafo.json";
	
	public ManejadorArchivos() {
	}

	public void CargarVertices(Grafo<Integer, Interseccion, Costo> grafo) {
		try (BufferedReader br = new BufferedReader(new FileReader(BOGOTA_VERTICES))) {
			br.readLine();
			String linea = br.readLine();
			while (linea != null) {
				Interseccion interseccionNueva = GenerarInterseccion(linea);
				grafo.addVertice(interseccionNueva.getIDNodo(), interseccionNueva);
				linea = br.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void CargarArcos(Grafo<Integer, Interseccion, Costo> grafo) {
		try (BufferedReader br = new BufferedReader(new FileReader(BOGOTA_ARCOS))) {
			br.readLine();
			String linea = br.readLine();
			while (linea != null) {
				String[] data = linea.split(" ");
				int origen = Integer.parseInt(data[0]);
				for (int i = 1; i < data.length; i++) {
					int destino = Integer.parseInt(data[i]);
					grafo.addArco(origen, destino, new Costo(grafo.getVertice(origen), grafo.getVertice(destino)));
				}
				linea = br.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void CargarCostos(Grafo<Integer, Interseccion, Costo> grafo) {
		try (BufferedReader br = new BufferedReader(new FileReader(TIEPOS_VIEAJES))) {
			br.readLine();
			String linea = br.readLine();
			while (linea != null) {
				String[] data = linea.split(",");
				int origen = Integer.parseInt(data[0]);
				int destino = Integer.parseInt(data[1]);
				double tiempoPromedio = Double.parseDouble(data[3]);
				Costo costo = grafo.getCosto(origen, destino);
				if (costo == null) {
					linea = br.readLine();
					continue;
				}
				CompletarCosto(costo, costo.getIdInterseccionOrigen(), costo.getIdInterseccionDestino(),
						tiempoPromedio);
				linea = br.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void crearArchivoJSON(Grafo<Integer, Interseccion, Costo> grafo) {
		JSONObject jsonGraph = new JSONObject();
		JSONArray intersecciones = new JSONArray();
		JSONArray adyacencias = new JSONArray();

		for (Interseccion vertice : grafo.getValues()) {
			JSONObject vertex = new JSONObject();
			vertex.put("id", vertice.getIDNodo());
			vertex.put("longitud", vertice.getLongitud());
			vertex.put("latitud", vertice.getLatitud());
			vertex.put("MOVEMENT_ID", vertice.getMOVEMENT_ID());
			intersecciones.add(vertex);
		}

		for (Costo costo : grafo.getCostos()) {
			JSONObject cost = new JSONObject();
			cost.put("origen", costo.getIdInterseccionOrigen().getIDNodo());
			cost.put("destino", costo.getIdInterseccionDestino().getIDNodo());
			cost.put("tiempo", costo.getCostTiempoDeViaje());
			cost.put("distancia", costo.getCostDistanciaHarversine());
			cost.put("velocidad", costo.getCostVelocidad());
			adyacencias.add(cost);
		}

		jsonGraph.put("vertices", intersecciones);
		jsonGraph.put("adyacencias", adyacencias);

		EscribirArchivo(GRAFO, jsonGraph.toJSONString());
	}

	public void cargrArchivoJSON(Grafo<Integer, Interseccion, Costo> grafo) {
		try {
			JSONObject jsonObject = (JSONObject) new JSONParser().parse(new FileReader(GRAFO));
			
			JSONArray intersecciones = (JSONArray) jsonObject.get("vertices");
			for(Object vertice : intersecciones)
			{
				Interseccion interseccionNueva = GenerarInterseccionJSON((JSONObject) vertice);
				grafo.addVertice(interseccionNueva.getIDNodo(), interseccionNueva);
			}
			
			JSONArray adjacentes = (JSONArray) jsonObject.get("adyacencias");
			for(Object costo : adjacentes)
			{
				JSONObject obj = (JSONObject) costo;
				int origen = (int) ((long) obj.get("origen"));
				int destino = (int) ((long) obj.get("destino"));
				double tiempo = (double) obj.get("tiempo");
				double distancia = (double) obj.get("distancia");
				Costo cost = new Costo(grafo.getVertice(origen), grafo.getVertice(destino), distancia, tiempo);
				grafo.addArco(origen, destino, cost);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	// Metodos Auxiliares
	private Interseccion GenerarInterseccion(String linea) {
		String[] data = linea.split(";");
		return new Interseccion(Integer.parseInt(data[0]), Double.parseDouble(data[1]), Double.parseDouble(data[2]),
				Integer.parseInt(data[3]));
	}
	
	private Interseccion GenerarInterseccionJSON(JSONObject obj) {
		int id = (int) ((long) obj.get("id"));
		double longitud = (double) obj.get("longitud");
		double latitud = (double) obj.get("latitud");
		int MOVEMENT_ID = (int) ((long) obj.get("MOVEMENT_ID"));
		return new Interseccion(id,longitud,latitud,MOVEMENT_ID);
	}

	private void CompletarCosto(Costo costo, Interseccion origen, Interseccion destino, double tiempoPromedio) {
		costo.addCostDistanciaHarversine(Haversine.distance(origen.getLatitud(), origen.getLongitud(),
				destino.getLatitud(), destino.getLongitud()));
		costo.addCostTiempoDeViaje(tiempoPromedio);
		costo.updateCostVelocidad();
	}

	private void EscribirArchivo(String rutaArchivom, String contenido) {
		try (FileWriter file = new FileWriter(rutaArchivom)) {
			file.write(contenido);
			file.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
