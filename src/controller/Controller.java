package controller;

import java.util.Comparator;
import java.util.Iterator;
import com.sun.javafx.collections.MappingChange.Map;
import data_structures.BFS;
import data_structures.DepthFirstPaths;
import data_structures.Grafo;
import data_structures.LinearProbingHashST;
import data_structures.MinPQ;
import data_structures.Queue;
import model.Costo;
import model.Haversine;
import model.Interseccion;
import model.InterseccionT;
import model.InterseccionVM;

public class Controller 
{
	//son dos m�s que la cantidad de vertices para eviar el rehash
	public final int CANTIDAD_VERTICES = 225896;
	public static final int CANTIDAD_ZONAS = 1147;

	private ManejadorArchivos manejadorArchivos;

	private Grafo<Integer, Interseccion, Costo> grafo;

	public Controller()
	{
		manejadorArchivos = new ManejadorArchivos();
		grafo = new Grafo<Integer, Interseccion, Costo>(CANTIDAD_VERTICES);
	}

	public void CargarArchivos()
	{
		print("inicio de cargue");
		manejadorArchivos.CargarVertices(grafo);
		print("Fin cargue vertices");
		print("Cantidad de nodos:\t"+grafo.getCantNodos());
		manejadorArchivos.CargarArcos(grafo);
		print("CAntidad de arcos:\t"+grafo.getCantArcos());
		print("Fin cargue");
	}

	public void CargarTiempos()
	{
		print("inicio de cargue");
		manejadorArchivos.CargarCostos(grafo);
		print("Fin cargue");
	}

	public void CargarGrafoJSON()
	{
		manejadorArchivos.crearArchivoJSON(grafo);
	}

	public void GenerarGrafoJSON()
	{
		grafo = new Grafo<Integer, Interseccion, Costo>(CANTIDAD_VERTICES);
		manejadorArchivos.cargrArchivoJSON(grafo);
	}

	public void MostrarMapa()
	{
		MostrarMapaGeneral(grafo, false);
	}

	private Maps MostrarMapaGeneral(Grafo<Integer, Interseccion, Costo> grafo, boolean monocromatico)
	{
		Maps maps = new Maps(grafo, monocromatico);
		maps.initFrame("Calles");
		return maps;
	}

	private void print(String linea)
	{
		System.out.println(linea);
	}

	/**
	 * Metodo que revisa cual es la minima distacion en unas cordenadas dadas
	 * por parametro y los diferentes valores del grafo
	 * @param latitud
	 * @param longitud
	 * @return el id del nodo mas cercano
	 */
	public int DarIDMasCercano(double latitud, double longitud)
	{
		Interseccion masCercana = null;
		double minDistancia = Integer.MAX_VALUE;

		for(Interseccion intersecion : grafo.getValues())
		{
			double distancia = Haversine.distance(latitud, latitud, intersecion.getLatitud(), intersecion.getLongitud());
			if(distancia < minDistancia)
			{
				minDistancia = distancia;
				masCercana = intersecion;
			}
		}

		if(masCercana == null)
			return -1;
		else
			return masCercana.getIDNodo();
	}

	/**
	 * Devuelve una interseccion dependiendo
	 * del id que fue pasado por parametro
	 * @param id
	 * @return la llave 
	 */

	public Interseccion darInterseccion(int id)
	{
		return grafo.getVertice(id);
	}
	/**
	 * da la menor velocidad de las n intersecciones pasadas por parametro
	 * @param n
	 * 
	 */
	public void darNMenorVelocidad(int n)
	{
		MinPQ<InterseccionVM> colaMenorVelocidad = new MinPQ<>(CANTIDAD_VERTICES, new Comparator<InterseccionVM>() {
			@Override
			public int compare(InterseccionVM o1, InterseccionVM o2) {
				return Double.compare(o1.getVelocidadMEdia(), o2.getVelocidadMEdia());
			}
		});
		for(Interseccion interseccion : grafo.getValues())
		{

		}
	}
	/**
	 * Este metodo se encarga de mostrar los vertices 
	 * a los que alcanza un vertice dado por parametro
	 * @param ID
	 * @param tiempo
	 */
	public 	Queue<InterseccionT>verticesAlcanzables(double latitud, double longitud,double tiempo){
		/**1. obtener los vertices adyacentes al id dado por parametro
		 *2.  
		 * 
		 */
		int IDVerticeInicial=DarIDMasCercano(latitud,  longitud);
		Queue<InterseccionT> colaRespuesta = new Queue<>();
		Queue<InterseccionT> laColaDeTrabajo =new Queue<>();
		LinearProbingHashST<Integer, Boolean> listaDeTrabajo = grafo.getListaTrabajo();
		//listaDeTrabajo.put(IDVerticeInicial, true);
		laColaDeTrabajo.enqueue(new InterseccionT(grafo.getVertice(IDVerticeInicial), 0));
		while(!laColaDeTrabajo.isEmpty()){
			InterseccionT Element = laColaDeTrabajo.dequeue();
			if(!listaDeTrabajo.get(Element.getInterseccio().getIDNodo())){
				listaDeTrabajo.put(Element.getInterseccio().getIDNodo(),true );
				colaRespuesta.enqueue(Element);
			}//cogemos un elemento en especial y bucamos los adyacientes
			for(int adyacentes: grafo.getAdyacentes(Element.getInterseccio().getIDNodo())){
				double tiempoFinal=grafo.getCosto(Element.getInterseccio().getIDNodo(), adyacentes).getCostTiempoDeViaje();
				if(!listaDeTrabajo.get(adyacentes)&& tiempoFinal+Element.gettiempo()<=tiempo){

					laColaDeTrabajo.enqueue(new InterseccionT(grafo.getVertice(IDVerticeInicial), tiempoFinal+Element.gettiempo()));
				}
			}
		}
		Queue<InterseccionT>colaAuxiliar= new Queue<>();
		Queue<Integer>colaId= new Queue<>();
		for(InterseccionT iter: colaRespuesta){
			colaAuxiliar.enqueue(iter);
			colaId.enqueue(iter.getInterseccio().getIDNodo());
		}
		Maps mapa =  MostrarMapaGeneral(grafo.subgrafo(colaId), true);
		mapa.resaltarPunto(IDVerticeInicial);
		return colaAuxiliar;
	}


	public Iterator<Integer> R4(double platitudOrigen,double plongitudOreigen,double platitudDestino,double plongitudDestino)
	{


		int IdInterseccionOrigen = DarIDMasCercano(platitudOrigen, plongitudOreigen);
		int IdInterseccionDestino = DarIDMasCercano(platitudDestino, plongitudDestino);

		DepthFirstPaths a = new DepthFirstPaths(grafo, IdInterseccionOrigen);
		Iterator<Integer> iter = a.pathTo(IdInterseccionDestino).iterator();



		return iter;


	}


	public void  R5(int n)
	{
		MinPQ<InterseccionVM> colaMenorVelocidad = darMenorVelocidad();
		int idMasPequeno = -1;
		Queue<Integer> colaElementosMenores = new Queue<>();
		for (int i = 0; i < n; i++) {
			InterseccionVM elemento = colaMenorVelocidad.delMin();
			if(i == 0)
				idMasPequeno = elemento.getInterseccio().getIDNodo();
			print(elemento.getInterseccio().toString() + " velocidad promedio: " + elemento.getVelocidadMEdia());
			colaElementosMenores.enqueue(elemento.getInterseccio().getIDNodo());
		}

		Grafo<Integer, Interseccion, Costo> subgrafo = grafo.subgrafo(colaElementosMenores);
		LinearProbingHashST<Integer, Boolean> listaTrabajo = subgrafo.getListaTrabajo();
		int numeroComponente = 0;
		for(int llave : subgrafo.getKeys())
		{
			if(!listaTrabajo.get(llave))
			{
				print("Componente "+(numeroComponente++));
				Grafo<Integer, Interseccion, Costo> componente = BFS.bfs(subgrafo, llave);
				String listaComponente = "";
				for(int id : componente.getKeys())
				{
					listaComponente += id+" - ";
					listaTrabajo.put(id, true);
				}
				print(listaComponente);
			}
		}

		Maps mapa =  MostrarMapaGeneral(subgrafo, true);
		mapa.resaltarPunto(idMasPequeno);
	}

	public MinPQ<InterseccionVM> darMenorVelocidad()
	{
		MinPQ<InterseccionVM> colaMenorVelocidad = new MinPQ<>(CANTIDAD_VERTICES, new Comparator<InterseccionVM>() {
			@Override
			public int compare(InterseccionVM o1, InterseccionVM o2) {
				return Double.compare(o1.getVelocidadMEdia(), o2.getVelocidadMEdia());
			}
		});

		for(Interseccion interseccion : grafo.getValues())
		{
			int cantidad = 0;
			double velocidad = 0;
			for (int llaveAdyacente : grafo.getAdyacentes(interseccion.getIDNodo())) 
			{
				velocidad += grafo.getCosto(interseccion.getIDNodo(), llaveAdyacente).getCostVelocidad();
				cantidad++;
			}
			colaMenorVelocidad.insert(new InterseccionVM(interseccion, velocidad/cantidad));
		}

		return colaMenorVelocidad;
	}



	public Interseccion[]  R6()
	{

		int tam=0;
		Interseccion[] resp = new Interseccion[tam];

		return resp;

	}
	public Interseccion[]  R7(int platitudOrigen,int plongitudOreigen,int platitudDestino,int plongitudDestino)
	{
		int tam=0;
		Interseccion[] resp = new Interseccion[tam];

		return resp;

	}
	public void  R8(double latitud, double longitud, int tiempo)

	{

	}

	public Interseccion[]  R9()
	{
		int tam=0;
		Interseccion[] resp = new Interseccion[tam];

		return resp;
	}
	public void  R10()
	{

	}
	public void  R11()
	{

	}
	public void  R12()
	{

	}




}
