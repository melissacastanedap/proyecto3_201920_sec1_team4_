package data_structures;

import java.util.Stack;

import model.Costo;
import model.Interseccion;

public class DepthFirstPaths
{	
	    private boolean[] marked;    
	    private int[] edgeTo;        
	    private final int s;       

	    /**
	     * Computes a path between {@code s} and every other vertex in graph {@code G}.
	     */
	    public DepthFirstPaths(Grafo<Integer, Interseccion, Costo> G, int s) 
	    {
	        this.s = s;
	        edgeTo = new int[G.getCantNodos()];
	        marked = new boolean[G.getCantNodos()];
	        dfs(G, s);
	    }

	    
	    private void dfs(Grafo<Integer, Interseccion, Costo> G, int v) {
	        marked[v] = true;

	        for (int w : G.getAdyacentes(v) ) {
	            if (!marked[w]) {
	                edgeTo[w] = v;
	                dfs(G, w);
	            }
	        }
	    }

	    /**
	     * Is there a path between the source vertex {@code s} and vertex {@code v}?
	     */
	    public boolean hasPathTo(int v) {
	        validateVertex(v);
	        return marked[v];
	    }

	    /**
	     * Returns a path between the source vertex {@code s} and vertex {@code v}, or
	     * {@code null} if no such path.
	     */
	    public Iterable<Integer> pathTo(int v) {
	        validateVertex(v);
	        if (!hasPathTo(v)) return null;
	        
	        Stack<Integer> path = new Stack<Integer>();
	        
	        
	        for (int x = v; x != s; x = edgeTo[x])
	            path.push(x);
	        path.push(s);
	        return path;
	    }

	   
	    private void validateVertex(int v) {
	        int V = marked.length;
	        if (v < 0 || v >= V)
	            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
	    }

	    

	}


