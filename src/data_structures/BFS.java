package data_structures;

public class BFS
{
	public static <K,V,C> Grafo<K,V,C> bfs(Grafo<K,V,C> grafo, K s)
	{
		/**
		 * Esta clase fue modificada por nosotros para que se adecuara a nuestros requerimientos 
		 */
		//Incializamos el grafo
		Grafo<K,V,C> grafoRespuesta = new Grafo<>();
		//Incializaos la cola de prioridad
		Queue<K> cola = new Queue<>();
		//Inicializamos una hash
		LinearProbingHashST<K, Boolean> listaTrabajo = grafo.getListaTrabajo();
		// A la lista de hash  le agregamos la key y el valor verdadero
		listaTrabajo.put(s,true);
		// En la cola guardamos la llave
		cola.enqueue(s);
		
		//Mientras la cola no este vacia
		while(!cola.isEmpty())
		{
			//sacamos el objeto de tipo generico de la cola
			K e = cola.dequeue();
			//Si la llave de e es nula
			if(grafoRespuesta.getVertice(e) == null)
				//agregamos el vertice por su llave y su valor
				grafoRespuesta.addVertice(e, grafo.getVertice(e));
			//Obtenemos los adyacentes de e
			for(K adyacente : grafo.getAdyacentes(e))
			{
				//A cada uno de los adyacentes si no estan en la lista del trabajo 
				if(!listaTrabajo.get(adyacente))
				{
					//agregamos a la lista de trabajo el adyacente
					listaTrabajo.put(adyacente, true);
					//Metemos en la cola en adyacente
					cola.enqueue(adyacente);
					//Si el no contiene el adyacente
					if(grafoRespuesta.getVertice(adyacente) == null)
						//Agregamos este vertice con su key y valor
						grafoRespuesta.addVertice(e, grafo.getVertice(e));
					//agregamos el arco con con ini, final, dar el costo de el arco 
					grafoRespuesta.addArco(e, adyacente, grafo.getCosto(e, adyacente));
					
				}
			}
		}
		//se devuelve el grafo respuesta
		return grafoRespuesta;
	}	
}
