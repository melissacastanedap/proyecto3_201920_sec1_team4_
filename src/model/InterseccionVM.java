package model;

public class InterseccionVM 
{
	private Interseccion interseccio;
	private  double velocidadMEdia;
	
	public InterseccionVM(Interseccion interseccio, double velocidadMEdia) {
		this.interseccio = interseccio;
		this.velocidadMEdia = velocidadMEdia;
	}

	public Interseccion getInterseccio() {
		return interseccio;
	}

	public void setInterseccio(Interseccion interseccio) {
		this.interseccio = interseccio;
	}

	public double getVelocidadMEdia() {
		return velocidadMEdia;
	}

	public void setVelocidadMEdia(double velocidadMEdia) {
		this.velocidadMEdia = velocidadMEdia;
	}
	
}
