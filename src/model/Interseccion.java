package model;

public class Interseccion 
{
	
	private int IDNodo;
	private double longitud;
	private double latitud;
	private int MOVEMENT_ID;
	
	public Interseccion()
	{}
	
	public Interseccion(int iDNodo, double longitud, double latitud, int mOVEMENT_ID) {
		IDNodo = iDNodo;
		this.longitud = longitud;
		this.latitud = latitud;
		MOVEMENT_ID = mOVEMENT_ID;
	}

	public int getIDNodo() {
		return IDNodo;
	}

	public void setIDNodo(int iDNodo) {
		IDNodo = iDNodo;
	}

	public double getLongitud() {
		return longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	public double getLatitud() {
		return latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public int getMOVEMENT_ID() {
		return MOVEMENT_ID;
	}

	public void setMOVEMENT_ID(int mOVEMENT_ID) {
		MOVEMENT_ID = mOVEMENT_ID;
	}

	@Override
	public String toString() {
		return "Interseccion [IDNodo=" + IDNodo + ", longitud=" + longitud + ", latitud=" + latitud + ", MOVEMENT_ID="
				+ MOVEMENT_ID + "]";
	}

}
